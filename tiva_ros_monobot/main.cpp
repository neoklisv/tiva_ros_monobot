#define RX_BUFFER_SIZE 512
#define TX_BUFFER_SIZE 512


#include <stdbool.h>
#include <stdint.h>
#include <stdio.h>
#include "driverlib/fpu.h"
#include <math.h>
#include "inc/hw_memmap.h"
#include "inc/hw_types.h"
#include "driverlib/sysctl.h"
#include "driverlib/gpio.h"
#include "driverlib/debug.h"
#include "driverlib/pwm.h"
#include "driverlib/pin_map.h"
#include "inc/hw_gpio.h"
#include "driverlib/uart.h"
#include "driverlib/qei.h"


// interrupt-timer definitions

#include "driverlib/interrupt.h"
 #include "inc/tm4c1294ncpdt.h"
  #include "driverlib/timer.h"

// TivaC specific includes
extern "C"
{
  #include <driverlib/sysctl.h>
  #include <driverlib/gpio.h>

}
// ROS includes
#include <ros.h>
#include <std_msgs/String.h>
#include <std_msgs/Int32.h>
#include <std_msgs/Float32.h>


uint8_t ui8PinData=1;

//MOTOR STUFF
#define PWM_FREQUENCY 50000 //50KHz PWM Frequency
#define APP_PI 3.1415926535897932384626433832795f
#define STEPS 256


#define DownLimit 10 //Minimum PWM output value
#define UpLimit 90 //MAximum PWM output value
int Setpoint = 300; //101949=1999*51 define Setpoint for motor command (1:51 is the transmission ratio of the speed reducer).
 //With this command I expect a full rotation of the shaft. Tip:Command must be positive value for this code
//note: I have set it to 300 for home testing



int error=0; //error=Desired Angle(in Counts)- Position(counts-measured Value of Angle from encoder)
#define Kp 10 //define Kp gain of PD Controller 17
#define Kd 0.05 //define Kd gain

float P_term=0 ;
float D_term=0 ;
float u=0 ; //output command(%)


volatile int32_t Direction; //Direction of motor shaft
volatile int32_t Velocity; //Velocity of motor shaft in counts/period
volatile uint32_t Position; //Position of motor shaft in counts (360 degrees -> 1999 counts)
volatile int response[10000];
volatile int response_index=0;
int LastPosition=0;

void hbridge();
void softbridge();
volatile uint32_t ui32Load;


// ROS nodehandle
ros::NodeHandle nh;
std_msgs::Float32 str_msg;
std_msgs::Int32 cur_msg;
ros::Publisher force_pub("spring_force", &str_msg);
ros::Publisher motor_pub("cur_pos", &cur_msg);

void handle_motor(int pos){


}

void handle_force(){
    float force = 55555.55;
    /*
    char hello[13] = "Hello world!";
    char buf[32];
    snprintf(buf, sizeof buf, "%s%s%d", hello, " ", msg.data);

      str_msg.data = buf;
         force_pub.publish(&str_msg);

        */
    str_msg.data = force+Setpoint;
    force_pub.publish(&str_msg);

   cur_msg.data = Position;
   motor_pub.publish(&cur_msg);

}


void ros_handler(const std_msgs::Int32& msg)
{
    Setpoint = msg.data;
    /*
char hello[13] = "Hello world!";
char buf[32];
snprintf(buf, sizeof buf, "%s%s%d", hello, " ", msg.data);

  str_msg.data = buf;
     force_pub.publish(&str_msg);
*/
}

ros::Subscriber<std_msgs::Int32> pwm_sub("motor_pos", &ros_handler);


int main(void)
{
  // TivaC application specific code
  MAP_FPUEnable();
  MAP_FPULazyStackingEnable();


  volatile uint32_t ui32PWMClock;
  volatile uint32_t ui32SysClkFreq;
  volatile uint32_t ui32Period;


  //CPU clock setupwrong checksum for msg length, length 25860
  ui32SysClkFreq = SysCtlClockFreqSet((SYSCTL_XTAL_25MHZ | SYSCTL_OSC_MAIN |
  SYSCTL_USE_PLL | SYSCTL_CFG_VCO_480), 120000000);


  // ROS nodehandle initialization and topic registration
  nh.initNode();

  nh.advertise(motor_pub);
  nh.advertise(force_pub);
  nh.subscribe(pwm_sub);


  //ENABLING VARIOUS PERIPHERALS
  SysCtlPeripheralEnable(SYSCTL_PERIPH_GPIOG);
  SysCtlPeripheralEnable(SYSCTL_PERIPH_GPIOF);
  SysCtlPeripheralEnable(SYSCTL_PERIPH_PWM0);
  SysCtlPeripheralEnable(SYSCTL_PERIPH_TIMER0);




  //QEI SETUP
  SysCtlPeripheralEnable(SYSCTL_PERIPH_GPIOL); // Enable the GPIOL peripheral for the input signals of the Encoder
  SysCtlPeripheralEnable(SYSCTL_PERIPH_QEI0); // Enable the QEI0 peripheral
  SysCtlDelay(10); //wait until it is ready
   /* Configure GPIO pins.*/
   GPIOPinConfigure(GPIO_PL1_PHA0); //Pin PC5 as PhA1
  GPIOPinConfigure(GPIO_PL2_PHB0); //Pin PC6 as PhB1
   GPIOPinTypeQEI(GPIO_PORTL_BASE,GPIO_PIN_1 |GPIO_PIN_2); //Pins PC5 & PC6 for the QEI module
   QEIDisable(QEI0_BASE);
   QEIVelocityDisable(QEI0_BASE);
   QEIIntDisable(QEI0_BASE, (QEI_INTERROR | QEI_INTDIR | QEI_INTTIMER |
  QEI_INTINDEX));
   QEIConfigure(QEI0_BASE, (QEI_CONFIG_CAPTURE_A_B | QEI_CONFIG_NO_RESET
  | QEI_CONFIG_QUADRATURE | QEI_CONFIG_NO_SWAP), 900);
   SysCtlDelay(10);
  //wait until it is ready
   QEIVelocityConfigure(QEI0_BASE, QEI_VELDIV_16, 40000);
  //40000 is the period at which the velocity will be measured
   SysCtlDelay(10);
  //wait until it is ready
   QEIPositionSet(QEI0_BASE, 0);
   SysCtlDelay(10);
  //wait until it is ready
   QEIEnable(QEI0_BASE);
   SysCtlDelay(10);
  //wait until it is ready
   QEIVelocityEnable(QEI0_BASE);
   SysCtlDelay(10);



  //PWM Setup (double for hardware hbridge)
  PWMClockSet(PWM0_BASE,PWM_SYSCLK_DIV_64);
  GPIOPinConfigure(GPIO_PF2_M0PWM2);
  GPIOPinTypePWM(GPIO_PORTF_BASE, GPIO_PIN_2);
  GPIOPinConfigure(GPIO_PF3_M0PWM3);
  GPIOPinTypePWM(GPIO_PORTF_BASE, GPIO_PIN_3);


  ui32PWMClock = ui32SysClkFreq / 64;
  ui32Load = (ui32PWMClock / PWM_FREQUENCY) - 1;
  PWMGenConfigure(PWM0_BASE, PWM_GEN_1, PWM_GEN_MODE_DOWN);
  PWMGenPeriodSet(PWM0_BASE, PWM_GEN_1, ui32Load);
  PWMPulseWidthSet(PWM0_BASE, PWM_OUT_2, ui32Load/2);
  PWMOutputState(PWM0_BASE, PWM_OUT_2_BIT, true);
  PWMPulseWidthSet(PWM0_BASE, PWM_OUT_3, ui32Load/2);
  PWMOutputState(PWM0_BASE, PWM_OUT_3_BIT, true);
  PWMGenEnable(PWM0_BASE, PWM_GEN_1);



  //timer setup
  ui32Period = ui32SysClkFreq/2000; //2KHz timer frequency
  TimerLoadSet(TIMER0_BASE, TIMER_A, ui32Period -1);
  IntEnable(INT_TIMER0A);
  TimerIntEnable(TIMER0_BASE, TIMER_TIMA_TIMEOUT);
  IntMasterEnable();
  TimerEnable(TIMER0_BASE, TIMER_A);





  SysCtlPeripheralEnable(SYSCTL_PERIPH_GPION);
  GPIOPinTypeGPIOOutput(GPIO_PORTN_BASE, GPIO_PIN_0|GPIO_PIN_1);
  GPIOPinWrite(GPIO_PORTN_BASE, GPIO_PIN_0|GPIO_PIN_1, 0x00);

  /*
for (int i=0; i<5; i++)
  {
  GPIOPinWrite(GPIO_PORTN_BASE, GPIO_PIN_0 | GPIO_PIN_1, ui8PinData);
  SysCtlDelay(2000000);
  if(ui8PinData==4) {ui8PinData=1;} else {ui8PinData=ui8PinData*2;}
  }
*/
  while (1)
   {

     // Handle all communications and callbacks.

    // handle_motor(pos); //tell motor to go to position "pos"

     handle_force(); //send force sensor data

     nh.spinOnce();

     // Delay for a bit.
    nh.getHardware()->delay(10);
   }

}
extern "C"
{
void Timer0IntHandler(void)
{
    // Clear the timer interrupt
    TimerIntClear(TIMER0_BASE, TIMER_TIMA_TIMEOUT);
/*
    GPIOPinWrite(GPIO_PORTN_BASE, GPIO_PIN_0 | GPIO_PIN_1, ui8PinData);
            //   SysCtlDelay(2000000);
               if(ui8PinData==4) {ui8PinData=1;} else {ui8PinData=ui8PinData*2;}
*/


   hbridge(); //change to softbridge for the lab motor controller!!
}
}

void softbridge(){
    /* Get direction (1 = forward, -1 = backward). */
     Direction = QEIDirectionGet(QEI0_BASE);
    /* Get velocity(counts per period specified) and multiply by
    direction so that it is signed.*/
     Velocity = QEIVelocityGet(QEI0_BASE)*Direction;
    /* Get absolute position in counts.*/
     Position = QEIPositionGet(QEI0_BASE);

     if (response_index<10000){
                 response[response_index]=Position;
                 response_index++;
                 }

                 /*

                 if (Position != LastPosition){
                         //LastPosition = Position;
                         //response[response_index]=Position;
                        //response_index++;
                         //if (response_index==9999) response_index=0;
                        UARTsendint(Position);
                         UARTCharPut(UART0_BASE, 10);
                        UARTCharPut(UART0_BASE, 13);
                         }
                 */

    /* PD Control Algorithm */
     error=Setpoint-Position;
     P_term=Kp*error;
     D_term=Kd*(-Velocity); //Using Velocity from encoder
     u=P_term+D_term; //Output Command
     if (u<0){
     u=-u;
     GPIOPinWrite(GPIO_PORTF_BASE, GPIO_PIN_2, 0);
     } else{
     GPIOPinWrite(GPIO_PORTF_BASE, GPIO_PIN_2, 4);
     }
     if (u>=UpLimit){
     u=UpLimit;
     }
     else if (u<=DownLimit){
     u=DownLimit;
     }
     PWMPulseWidthSet(PWM0_BASE, PWM_OUT_4, u/100*ui32Load);
    //map the PWM signal depending on the output command u
}

void hbridge(){
    /* Get direction (1 = forward, -1 = backward). */
              Direction = QEIDirectionGet(QEI0_BASE);
             /* Get velocity(counts per period specified) and multiply by
             direction so that it is signed.*/
              Velocity = QEIVelocityGet(QEI0_BASE)*Direction;
             /* Get absolute position in counts.*/
              Position = QEIPositionGet(QEI0_BASE);


              if (response_index<10000){
              response[response_index]=Position;
              response_index++;
              }

              /*

              if (Position != LastPosition){
                      //LastPosition = Position;
                      //response[response_index]=Position;
                     //response_index++;
                      //if (response_index==9999) response_index=0;
                     UARTsendint(Position);
                      UARTCharPut(UART0_BASE, 10);
                     UARTCharPut(UART0_BASE, 13);
                      }
              */

             /* PD Control Algorithm */
              error=Setpoint-Position;
              P_term=Kp*error;
              D_term=Kd*(-Velocity); //Using Velocity from encoder
              u=P_term+D_term; //Output Command

              if (u>=0){
              if (u>=UpLimit){
                      u=UpLimit;
                      }
                      else if (u<=DownLimit){
                      u=DownLimit;
                      }
              }
              else{
                u=-u;
                 if (u>=UpLimit){
                                  u=UpLimit;
                                  }
                                  else if (u<=DownLimit){
                                  u=DownLimit;
                                  }
                 u=-u;

              }


              if (u<0){
              u=-u;
              PWMPulseWidthSet(PWM0_BASE, PWM_OUT_3, u/100*ui32Load);
              PWMPulseWidthSet(PWM0_BASE, PWM_OUT_2, 1);

              } else{
                  PWMPulseWidthSet(PWM0_BASE, PWM_OUT_2, u/100*ui32Load);
                  PWMPulseWidthSet(PWM0_BASE, PWM_OUT_3, 1);
              }

}
